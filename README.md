# README #

Aplikacja służy do wyliczania rocznego zysku netto dla instrumentów finansowych na podstawie listy transakcji kupna/sprzedaży.

## /rest/v1/financialData ##
### Dane wejściowe ###
Dane wejściowe powinny być dostarczone zapytaniem POST w formacie JSON.

* Lista
    * TIME - data transakcji w formacie `dd.MM.yyyy hh:mm:ss`
    * CODE - kod instrumentu finansowego
    * MARKET - kod giełdy
    * K/S - typ transakcji (K - kupno, S - sprzedaż)
    * VOLUME - liczba kupowanych lub sprzedawanych akcji
    * PRICE - cena pojedynczej akcji
    * CURRENCY - waluta, którą posługuje się giełda. (Wspierane waluty: PLN)


Przykładowe zapytanie:
```json
[
      {
        "TIME": "18.11.2019 11:58:05",
        "CODE": "A",
        "MARKET": "WWA-GPW",
        "K/S": "S",
        "VOLUME": 10,
        "PRICE": 1.415499,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "18.11.2019 11:49:42",
        "CODE": "A",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": "1,500",
        "PRICE": 1.435577,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "07.11.2019 09:01:02",
        "CODE": "B",
        "MARKET": "WWA-GPW",
        "K/S": "S",
        "VOLUME": 46,
        "PRICE": 91.94003,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "07.11.2019 09:15:06",
        "CODE": "C",
        "MARKET": "WWA-GPW",
        "K/S": "S",
        "VOLUME": 1000,
        "PRICE": 4.10999,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "28.05.2019 16:46:22",
        "CODE": "D",
        "MARKET": "WWA-GPW",
        "K/S": "S",
        "VOLUME": 1980,
        "PRICE": 16.9337,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "29.10.2019 16:49:29",
        "CODE": "B",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 46,
        "PRICE": 86.516102,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "04.04.2019 15:57:02",
        "CODE": "C",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 6500,
        "PRICE": 4.075834,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "20.03.2019 09:34:49",
        "CODE": "D",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 1300,
        "PRICE": 15.811425,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "08.03.2019 11:24:24",
        "CODE": "E",
        "MARKET": "WWA-GPW",
        "K/S": "S",
        "VOLUME": "1,500",
        "PRICE": 4.293191,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "05.02.2019 09:54:29",
        "CODE": "E",
        "MARKET": "WWA-GPW",
        "K/S": "S",
        "VOLUME": "1,100",
        "PRICE": 4.136068,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "07.01.2019 09:53:12",
        "CODE": "E",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 2100,
        "PRICE": 4.00233,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "03.01.2019 09:53:12",
        "CODE": "E",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 500,
        "PRICE": 4.136068,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "19.12.2018 15:15:21",
        "CODE": "F",
        "MARKET": "WWA-GPW",
        "K/S": "S",
        "VOLUME": 6500,
        "PRICE": 4.91089,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "18.12.2018 15:00:27",
        "CODE": "D",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 680,
        "PRICE": 15.76123,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "28.11.2018 16:36:19",
        "CODE": "F",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 6500,
        "PRICE": 4.768525,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "22.03.2018 12:14:16",
        "CODE": "C",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 3300,
        "PRICE": 4.563971,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "02.02.2018 16:06:27",
        "CODE": "C",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 3300,
        "PRICE": 4.467355,
        "CURRENCY": "PLN"
      },
      {
        "TIME": "31.01.2018 12:12:15",
        "CODE": "G",
        "MARKET": "WWA-GPW",
        "K/S": "K",
        "VOLUME": 530,
        "PRICE": 50.117269,
        "CURRENCY": "PLN"
      }
    ]
```
### Dane wyjściowe ###

* Lista
    * year - rok dla którego obliczony został zysk netto
    * code - kod instumentu finansowego
    * profit - zysk netto dla danego roku
    
Przykładowa odpowiedź:
```json
[
  {
    "year": 2019,
    "code": "A",
    "profit": -3.26
  },
  {
    "year": 2019,
    "code": "B",
    "profit": 217.49
  },
  {
    "year": 2019,
    "code": "C",
    "profit": -390.82
  },
  {
    "year": 2019,
    "code": "D",
    "profit": 2003.51
  },
  {
    "year": 2019,
    "code": "E",
    "profit": 432.83
  },
  {
    "year": 2018,
    "code": "F",
    "profit": 680
  }
]
```

### Dodatkowe założenia ###

* Liczba sprzedawanych akcji nie może być większa niż liczba posiadanych akcji
* Zaokrąglenie wykonywane jest dopiero przy wyświetlaniu danych
* Do każdej transakcji doliczany jest dodatkowy koszt w wysokości 0.39% kwoty transakcji, nie mniejszy niż 3 PLN
* Zysk netto liczony jest metodą FIFO 
(kwota sprzedaży pomniejszona o koszt transakcji oraz koszt zakupu akcji z uwzględnieniem zamortyzowanego kosztu transakcji zakupu)
* Nie można kupować części ułamkowych akcji
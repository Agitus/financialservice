/*
 * Created on 06-04-2021 16:34 by Karol Kaczorowski
*/

package pl.sii.financialservice.infrastructure;

import java.util.HashMap;
import java.util.Map;

import pl.sii.financialservice.domain.MarketInstrument;


class InMemoryDatabase {
	
	static Map<String, MarketInstrument> marketInstrumentDatabase = new HashMap<>();
}

/*
 * Created on 06-04-2021 16:35 by Karol Kaczorowski
*/

package pl.sii.financialservice.infrastructure;

import static pl.sii.financialservice.infrastructure.InMemoryDatabase.marketInstrumentDatabase;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import pl.sii.financialservice.application.queries.InstrumentProfitDto;
import pl.sii.financialservice.application.queries.InstrumentProfitDtoFactory;
import pl.sii.financialservice.application.queries.MarketInstrumentQueries;


@Repository
public class InMemoryMarketInstrumentQueries implements MarketInstrumentQueries {
	
	
	private final InstrumentProfitDtoFactory instrumentProfitDtoFactory;
	
	
	private InMemoryMarketInstrumentQueries() {
		
		this.instrumentProfitDtoFactory = new InstrumentProfitDtoFactory();
	}
	

	@Override
	public List<InstrumentProfitDto> getAllInstrumentProfits() {

		return marketInstrumentDatabase.values().stream()
			.map(instrumentProfitDtoFactory::createInstrumentProfitList)
			.flatMap(List::stream)
			.sorted()
			.collect(Collectors.toList());
	}
}

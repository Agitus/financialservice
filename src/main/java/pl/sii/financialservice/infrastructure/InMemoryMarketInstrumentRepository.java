/*
 * Created on 06-04-2021 16:36 by Karol Kaczorowski
*/

package pl.sii.financialservice.infrastructure;

import static pl.sii.financialservice.infrastructure.InMemoryDatabase.marketInstrumentDatabase;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import pl.sii.financialservice.domain.MarketInstrument;
import pl.sii.financialservice.domain.MarketInstrumentRepository;


@Repository
public class InMemoryMarketInstrumentRepository implements MarketInstrumentRepository {
	
	@Override
	public void clear() {
		
		marketInstrumentDatabase.clear();
	}

	@Override
	public Optional<MarketInstrument> findInstrument(String market, String instrumentCode) {
		
		String key = String.format("%s-%s", market, instrumentCode);

		return Optional.ofNullable(marketInstrumentDatabase.get(key));
	}

	@Override
	public void saveOrUpdateInstrument(MarketInstrument instrument) {

		String key = String.format("%s-%s", instrument.getMarket(), instrument.getCode());
		
		marketInstrumentDatabase.put(key, instrument);
	}
}

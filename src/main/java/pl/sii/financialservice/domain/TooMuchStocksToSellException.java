/*
 * Created on 06-04-2021 19:52 by Karol Kaczorowski
 */

package pl.sii.financialservice.domain;

public class TooMuchStocksToSellException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public TooMuchStocksToSellException(String message) {

		super(message);
	}
}

/*
 * Created on 06-04-2021 16:29 by Karol Kaczorowski
*/

package pl.sii.financialservice.domain;

import java.util.Optional;


public interface MarketInstrumentRepository {
	
	void clear();
	
	Optional<MarketInstrument> findInstrument(String market, String instrumentCode);
	
	void saveOrUpdateInstrument(MarketInstrument instrument);
}

/*
 * Created on 06-04-2021 18:10 by Karol Kaczorowski
*/

package pl.sii.financialservice.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.util.Assert;

import pl.sii.financialservice.common.Money;


public class Stocks implements Serializable, Comparable<Stocks> {

	private static final long serialVersionUID = 1L;

	private final LocalDateTime transactionDateTime;
	private final Money singleStockCost;
	private Long volume;
	
	
	Stocks(LocalDateTime transactionDateTime, Money singleStockCost, Long volume) {
		
		this.transactionDateTime = transactionDateTime;
		this.volume = volume;
		this.singleStockCost = singleStockCost;
	}

	public LocalDateTime getTransactionDateTime() {

		return transactionDateTime;
	}

	public Money getSingleStockCost() {

		return singleStockCost;
	}

	public Long getVolume() {

		return volume;
	}
	
	Money sellStocks(Long volume) {
		
		Assert.notNull(volume, "volume must not be null");
		Assert.isTrue(this.volume >= volume, "selling volume must not be greater than current volume");
		
		this.volume -= volume;
		
		return singleStockCost.multiply(volume);
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;

		if (!(o instanceof Stocks)) {
			return false;
		}

		Stocks stocks = (Stocks) o;

		return new EqualsBuilder()
			.append(transactionDateTime, stocks.transactionDateTime)
			.append(singleStockCost, stocks.singleStockCost)
			.append(volume, stocks.volume)
			.isEquals();
	}

	@Override
	public int hashCode() {

		return Objects.hash(transactionDateTime, singleStockCost, volume);
	}

	@Override
	public int compareTo(Stocks stocks) {

		return transactionDateTime.compareTo(stocks.transactionDateTime);
	}
}

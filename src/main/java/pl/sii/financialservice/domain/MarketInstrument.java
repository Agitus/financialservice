/*
 * Created on 06-04-2021 16:33 by Karol Kaczorowski
*/

package pl.sii.financialservice.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.util.Assert;

import pl.sii.financialservice.common.Money;


public class MarketInstrument implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String market;
	private final String code;
	private Long volume = 0L;
	private Queue<Stocks> ownedStocks = new PriorityQueue<>();
	private Map<Integer, Money> totalYearlyProfitMap = new HashMap<>();
	
	public MarketInstrument(String market, String code) {
		
		Assert.hasText(market, "market must not be empty");
		Assert.hasText(code, "code must not be empty");
		
		this.market = market;
		this.code = code;
	}

	public void buyStocks(Long volume, Money singleStockCost, LocalDateTime transactionDate) {
		
		Assert.notNull(volume, "volume must not be null");
		Assert.notNull(singleStockCost, "singleStockCost must not be null");
		
		this.volume += volume;
		ownedStocks.add(new Stocks(transactionDate, singleStockCost, volume));
	}

	public void sellStocks(Long volume, Money profit, int year) {

		Assert.notNull(volume, "volume must not be null");
		Assert.notNull(profit, "profit must not be null");
		
		if (this.volume < volume) {
			
			throw new TooMuchStocksToSellException(String.format("sell volume (%s) must not be greater than current volume (%s)", volume, this.volume));
		}

		Money cost = Money.of(BigDecimal.ZERO, profit.getCurrency());
		
		this.volume -= volume;
		
		while (volume > 0) {
			
			long sellVolume = Long.min(volume, ownedStocks.peek().getVolume());
			Stocks stocks = sellVolume < volume ? ownedStocks.poll() : ownedStocks.peek();
			cost = cost.add(stocks.sellStocks(sellVolume));
			volume -= sellVolume;
		}
		
		var netProfit = profit.subtract(cost);
		
		Money totalYearlyProfit = Optional.ofNullable(totalYearlyProfitMap.get(year))
			.map(yearlyProfit -> yearlyProfit.add(netProfit))
			.orElse(netProfit);
		
		totalYearlyProfitMap.put(year, totalYearlyProfit);
	}

	public String getMarket() {

		return market;
	}

	public String getCode() {

		return code;
	}

	public Map<Integer, Money> getTotalYearlyProfitMap() {

		return totalYearlyProfitMap;
	}

	Long getVolume() {

		return volume;
	}

	List<Stocks> getOwnedStocks() {

		return new ArrayList(ownedStocks);
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;

		if (!(o instanceof MarketInstrument)) {
			return false;
		}

		MarketInstrument that = (MarketInstrument) o;

		return new EqualsBuilder()
			.append(market, that.market)
			.append(code, that.code)
			.append(volume, that.volume)
			.append(ownedStocks, that.ownedStocks)
			.append(totalYearlyProfitMap, that.totalYearlyProfitMap)
			.isEquals();
	}

	@Override
	public int hashCode() {

		return Objects.hash(market, code, volume, ownedStocks, totalYearlyProfitMap);
	}
}

/*
 * Created on 06-04-2021 19:13 by Karol Kaczorowski
*/

package pl.sii.financialservice.common;

public enum Currency {
	PLN,
	USD,
	GBP
}

/*
 * Created on 06-04-2021 15:55 by Karol Kaczorowski
*/

package pl.sii.financialservice.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.util.Assert;


public class Money implements Serializable, Comparable<Money> {
	
	private static final long serialVersionUID = 1L;
	

	private BigDecimal value;
	private Currency currency;
	
	
	private Money(BigDecimal value, Currency currency) {
		
		Assert.notNull(value, "value must not be null");
		Assert.notNull(currency, "currency must not be null");
		
		this.value = value;
		this.currency = currency;
	}

	public static Money of(BigDecimal value, Currency currency) {
		
		return new Money(value, currency);
	}

	public BigDecimal getValue() {

		return value;
	}

	public Currency getCurrency() {

		return currency;
	}

	public Money add(Money augend) {

		Assert.notNull(augend, "augend must not be null");
		
		if (!currency.equals(augend.currency)) {
			
			throw new OperationOnDifferentCurrenciesException("currencies should be same");
		}

		return Money.of(value.add(augend.value), currency);
	}

	public Money subtract(Money subtrahend) {

		Assert.notNull(subtrahend, "money must not be null");

		if (!currency.equals(subtrahend.currency)) {

			throw new OperationOnDifferentCurrenciesException("currencies should be same");
		}

		return Money.of(value.subtract(subtrahend.value), currency);
	}

	public Money multiply(Long multiplicator) {

		Assert.notNull(multiplicator, "multiplicator must not be null");

		return multiply(new BigDecimal(multiplicator));
	}

	public Money multiply(BigDecimal multiplicator) {

		Assert.notNull(multiplicator, "multiplicator must not be null");

		return Money.of(value.multiply(multiplicator), currency);
	}

	public Money divide(Long divisor) {

		Assert.notNull(divisor, "divisor must not be null");
		Assert.isTrue(divisor != 0, "divisor must not be zero");

		return divide(new BigDecimal(divisor));
	}

	public Money divide(BigDecimal divisor) {

		Assert.notNull(divisor, "divisor must not be null");
		Assert.isTrue(!divisor.equals(BigDecimal.ZERO), "divisor must not be zero");

		return Money.of(value.divide(divisor), currency);
	}

	public Money max(Money other) {

		if (other == null) {
			
			return this;
		}
		
		return compareTo(other) >= 0 ? this : other;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;

		if (!(o instanceof Money)) {
			return false;
		}

		Money money = (Money) o;

		return new EqualsBuilder()
			.append(value.setScale(2, RoundingMode.HALF_UP), money.value.setScale(2, RoundingMode.HALF_UP))
			.append(currency, money.currency)
			.isEquals();
	}

	@Override
	public int hashCode() {

		return Objects.hash(value, currency);
	}

	@Override
	public int compareTo(Money money) {

		return value.compareTo(money.value);
	}

	@Override
	public String toString() {

		return String.format("%s %s", value, currency);
	}
}

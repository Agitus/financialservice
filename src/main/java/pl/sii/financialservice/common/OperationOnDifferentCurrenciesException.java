/*
 * Created on 06-04-2021 22:14 by Karol Kaczorowski
 */

package pl.sii.financialservice.common;

public class OperationOnDifferentCurrenciesException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public OperationOnDifferentCurrenciesException(String message) {

		super(message);
	}
}

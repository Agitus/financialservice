/*
 * Created on 06-04-2021 16:01 by Karol Kaczorowski
*/

package pl.sii.financialservice.interfaces;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import pl.sii.financialservice.application.command.TransactionCommand;
import pl.sii.financialservice.common.Money;


public class TransactionCommandFactory {
	
	List<TransactionCommand> createList(List<TransactionData> transactionDataList) {
		
		return transactionDataList.stream()
			.map(this::create)
			.collect(Collectors.toList());
	}
	
	private TransactionCommand create(TransactionData transactionData) {
		
		Assert.notNull(transactionData, "transactionData must not be null");

		Money stockPrice = Money.of(transactionData.getStockPrice(), transactionData.getCurrency());
		Long volume = Long.valueOf(transactionData.getVolume().replace(",", ""));
		
		return new TransactionCommand(transactionData.getCode(), 
			transactionData.getMarket(),
			transactionData.getDateTime(),
			transactionData.getTransactionType(),
			stockPrice, 
			volume);
	}
}

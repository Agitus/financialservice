/*
 * Created on 06-04-2021 18:48 by Karol Kaczorowski
*/

package pl.sii.financialservice.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import pl.sii.financialservice.common.Currency;
import pl.sii.financialservice.common.TransactionType;


class TransactionData implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty("TIME")
	@JsonFormat(pattern="dd.MM.yyyy HH:mm:ss")
	private LocalDateTime dateTime;
	
	@JsonProperty("CODE")
	private String code;
	
	@JsonProperty("MARKET")
	private String market;
	
	@JsonProperty("K/S")
	private TransactionType transactionType;
	
	@JsonProperty("VOLUME")
	private String volume;
	
	@JsonProperty("PRICE")
	private BigDecimal stockPrice;
	
	@JsonProperty("CURRENCY")
	private Currency currency;

	
	LocalDateTime getDateTime() {

		return dateTime;
	}

	String getCode() {

		return code;
	}

	String getMarket() {

		return market;
	}

	TransactionType getTransactionType() {

		return transactionType;
	}

	String getVolume() {

		return volume;
	}

	BigDecimal getStockPrice() {

		return stockPrice;
	}

	Currency getCurrency() {

		return currency;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;

		if (!(o instanceof TransactionData)) {
			return false;
		}

		TransactionData that = (TransactionData) o;

		return new EqualsBuilder()
			.append(dateTime, that.dateTime)
			.append(code, that.code)
			.append(market, that.market)
			.append(transactionType, that.transactionType)
			.append(volume, that.volume)
			.append(stockPrice, that.stockPrice)
			.append(currency, that.currency)
			.isEquals();
	}

	@Override
	public int hashCode() {

		return Objects.hash(dateTime, code, market, transactionType, volume, stockPrice, currency);
	}
}

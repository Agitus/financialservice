/*
 * Created on 06-04-2021 18:37 by Karol Kaczorowski
*/

package pl.sii.financialservice.interfaces;

public class ValidationErrorException extends IllegalArgumentException {

	private static final long serialVersionUID = 1L;

	public ValidationErrorException(String s) {
		
		super(s);
	}
}

/*
 * Created on 06-04-2021 18:36 by Karol Kaczorowski
*/

package pl.sii.financialservice.interfaces;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.sii.financialservice.application.command.TransactionApplicationService;
import pl.sii.financialservice.application.command.TransactionCommand;
import pl.sii.financialservice.application.queries.InstrumentProfitDto;
import pl.sii.financialservice.application.queries.MarketInstrumentQueries;


@RestController
@RequestMapping("/rest/v1/financialData")
class FinancialDataProcessingController {

	private final TransactionDataValidator transactionDataValidator;
	private final TransactionCommandFactory transactionCommandFactory;
	private final TransactionApplicationService transactionApplicationService;
	private final MarketInstrumentQueries marketInstrumentQueries;

	
	@Autowired
	private FinancialDataProcessingController(TransactionApplicationService transactionApplicationService, 
		MarketInstrumentQueries marketInstrumentQueries) {
		
		Assert.notNull(transactionApplicationService, "transactionApplicationService must not be null");
		Assert.notNull(marketInstrumentQueries, "marketInstrumentQueries must not be null");

		this.transactionDataValidator = new TransactionDataValidator();
		this.transactionCommandFactory = new TransactionCommandFactory();
		this.transactionApplicationService = transactionApplicationService;
		this.marketInstrumentQueries = marketInstrumentQueries;
	}

	
	@PostMapping
	private ResponseEntity<List<InstrumentProfitDto>> process(@RequestBody @Validated List<TransactionData> transactionDataList) {

		transactionDataValidator.validate(transactionDataList);
		
		List<TransactionCommand> transactionCommandList = transactionCommandFactory.createList(transactionDataList);
		transactionApplicationService.cleanAndProcessTransactions(transactionCommandList);
		
		List<InstrumentProfitDto> instrumentProfitList = marketInstrumentQueries.getAllInstrumentProfits();

		return ResponseEntity
			.ok()
			.body(instrumentProfitList);
	}
}

/*
 * Created on 06-04-2021 18:42 by Karol Kaczorowski
*/

package pl.sii.financialservice.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.sii.financialservice.common.OperationOnDifferentCurrenciesException;
import pl.sii.financialservice.domain.TooMuchStocksToSellException;


@ControllerAdvice(basePackageClasses = { FinancialDataProcessingController.class})
public class FinancialDataProcessingExceptionHandler {


	@ExceptionHandler(ValidationErrorException.class)
	@ResponseBody
	private ResponseEntity<String> validationErrorExceptionHandler(ValidationErrorException exception) {
		
		return ResponseEntity.unprocessableEntity()
			.body(exception.getMessage());		
	}

	@ExceptionHandler(TooMuchStocksToSellException.class)
	@ResponseBody
	private ResponseEntity<String> tooMuchStocksToSellExceptionHandler(TooMuchStocksToSellException exception) {

		return ResponseEntity.badRequest()
			.body(exception.getMessage());
	}

	@ExceptionHandler(OperationOnDifferentCurrenciesException.class)
	@ResponseBody
	private ResponseEntity<String> operationOnDifferentCurrenciesExceptionHandler(OperationOnDifferentCurrenciesException exception) {

		return ResponseEntity.badRequest()
			.body(exception.getMessage());
	}
}

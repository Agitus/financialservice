/*
 * Created on 06-04-2021 19:36 by Karol Kaczorowski
*/

package pl.sii.financialservice.interfaces;


import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.util.Assert;
import org.springframework.util.ObjectUtils;



class TransactionDataValidator {
	
	
	void validate(List<TransactionData> transactionDataList) {
		
		Assert.notNull(transactionDataList, "transactionDataList must not be null");
		
		String errorMessages = transactionDataList.stream()
			.map(this::validate)
			.flatMap(Set::stream)
			.distinct()
			.collect(Collectors.joining(",\n"));
		
		if (!errorMessages.isBlank()) {
			
			throw new ValidationErrorException(errorMessages);
		}
	}

	private Set<String> validate(TransactionData transactionData) {
		
		Set<String> errorsList = new HashSet<>();

		if (transactionData.getDateTime() == null) {

			errorsList.add("Date time should not be null");
		}
		else if (LocalDateTime.now().isBefore(transactionData.getDateTime())) {

			errorsList.add(String.format("Date time (%s) should not be in future", transactionData.getDateTime()));
		}
		
		if (ObjectUtils.isEmpty(transactionData.getCode())) {

			errorsList.add("code should be not empty");
		}
		
		if (ObjectUtils.isEmpty(transactionData.getMarket())) {

			errorsList.add("market should be not empty");
		}
		
		if (ObjectUtils.isEmpty(transactionData.getVolume())) {

			errorsList.add("volume should be not empty");
		}
		else if(!transactionData.getVolume().matches("[1-9][0-9,]*")) {

			errorsList.add(String.format("Volume (%s) should be positive integer", transactionData.getVolume()));
		}
		
		if (transactionData.getStockPrice() == null) {

			errorsList.add("Price should be not null");
		}
		
		return errorsList;
	}
	
	
}

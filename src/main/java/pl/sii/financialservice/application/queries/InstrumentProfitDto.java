/*
 * Created on 06-04-2021 18:48 by Karol Kaczorowski
*/

package pl.sii.financialservice.application.queries;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.util.Assert;


public class InstrumentProfitDto implements Serializable, Comparable<InstrumentProfitDto> {

	private static final long serialVersionUID = 1L;
	
	
	private final int year;
	private final String code;
	private final BigDecimal profit;

	
	public InstrumentProfitDto(int year, String code, BigDecimal profit) {

		Assert.notNull(code, "code must not be null");
		Assert.notNull(profit, "profit must not be null");

		this.year = year;
		this.code = code;
		this.profit = profit;
	}

	public int getYear() {

		return year;
	}

	public String getCode() {

		return code;
	}

	public BigDecimal getProfit() {

		return profit;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;

		if (o == null || getClass() != o.getClass())
			return false;

		InstrumentProfitDto that = (InstrumentProfitDto) o;

		return new EqualsBuilder()
			.append(year, that.year)
			.append(code, that.code)
			.append(profit, that.profit)
			.isEquals();
	}

	@Override
	public int hashCode() {

		return Objects.hash(year, code, profit);
	}

	@Override
	public int compareTo(InstrumentProfitDto instrumentProfitDto) {

		return new CompareToBuilder()
			.append(code, instrumentProfitDto.getCode())
			.append(year, instrumentProfitDto.getYear())
			.toComparison();
	}
}

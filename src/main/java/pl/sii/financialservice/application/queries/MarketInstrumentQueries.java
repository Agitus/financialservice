/*
 * Created on 06-04-2021 16:21 by Karol Kaczorowski
*/

package pl.sii.financialservice.application.queries;

import java.util.List;


public interface MarketInstrumentQueries {

	List<InstrumentProfitDto> getAllInstrumentProfits();
}

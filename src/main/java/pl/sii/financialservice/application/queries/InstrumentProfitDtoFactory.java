/*
 * Created on 06-04-2021 19:18 by Karol Kaczorowski
*/

package pl.sii.financialservice.application.queries;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import pl.sii.financialservice.common.Money;
import pl.sii.financialservice.domain.MarketInstrument;


public class InstrumentProfitDtoFactory {
	
	public List<InstrumentProfitDto> createInstrumentProfitList(MarketInstrument marketInstrument) {

		String instrumentCode = marketInstrument.getCode();
		Map<Integer, Money> totalYearlyProfitMap = marketInstrument.getTotalYearlyProfitMap();
		
		return totalYearlyProfitMap.keySet().stream()
			.map(key -> create(instrumentCode, key, totalYearlyProfitMap.get(key)))
			.collect(Collectors.toList());
	}
	
	private InstrumentProfitDto create(String instrumentCode, int year, Money money) {

		BigDecimal moneyWithoutCurrency = money.getValue().setScale(2, RoundingMode.HALF_UP);
		
		return new InstrumentProfitDto(year, instrumentCode, moneyWithoutCurrency);
	}
}

/*
 * Created on 06-04-2021 17:20 by Karol Kaczorowski
*/

package pl.sii.financialservice.application.command;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import pl.sii.financialservice.common.Money;
import pl.sii.financialservice.common.TransactionType;
import pl.sii.financialservice.domain.MarketInstrument;
import pl.sii.financialservice.domain.MarketInstrumentRepository;


@Service
public class TransactionApplicationService {
	
	private final MarketInstrumentRepository marketInstrumentRepository;

	
	@Autowired
	public TransactionApplicationService(MarketInstrumentRepository marketInstrumentRepository) {

		Assert.notNull(marketInstrumentRepository, "marketInstrumentRepository must not be null");
		
		this.marketInstrumentRepository = marketInstrumentRepository;
	}
	

	public void cleanAndProcessTransactions(List<TransactionCommand> transactionList) {
		
		marketInstrumentRepository.clear();
		
		transactionList.stream()
			.sorted()
			.forEachOrdered(this::processTransaction);
	}
	
	private void processTransaction(TransactionCommand transaction) {
		
		MarketInstrument marketInstrument = marketInstrumentRepository
			.findInstrument(transaction.getMarket(), transaction.getInstrumentCode())
			.orElseGet(() -> new MarketInstrument(transaction.getMarket(), transaction.getInstrumentCode()));
		
		Money transactionPrice = transaction.getStockPrice().multiply(transaction.getVolume());
		
		if (transaction.getTransactionType() == TransactionType.K) {

			Money singleStockTotalCost = singleStockCost(transactionPrice, transaction.getVolume());
			
			marketInstrument.buyStocks(transaction.getVolume(), singleStockTotalCost, transaction.getDateTime());
		}
		else {
			
			Money profit = transactionPrice.subtract(transactionExtraCost(transactionPrice));
			
			marketInstrument.sellStocks(transaction.getVolume(), profit, transaction.getDateTime().getYear());
		}
		
		marketInstrumentRepository.saveOrUpdateInstrument(marketInstrument);
	}

	Money singleStockCost(Money cost, Long volume) {
		
		return transactionTotalCost(cost).divide(volume);
	}
	
	Money transactionTotalCost(Money cost) {
		
		return cost.add(transactionExtraCost(cost));
	}
	
	Money transactionExtraCost(Money cost) {
		
		Money minimalTransactionCost = Money.of(BigDecimal.valueOf(3), cost.getCurrency());
		Money transactionCostFromPercent = cost.multiply(new BigDecimal("0.0039"));
		
		return minimalTransactionCost.max(transactionCostFromPercent);
	}
}

/*
 * Created on 06-04-2021 16:03 by Karol Kaczorowski
*/

package pl.sii.financialservice.application.command;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.util.Assert;

import pl.sii.financialservice.common.Money;
import pl.sii.financialservice.common.TransactionType;


public class TransactionCommand implements Serializable, Comparable<TransactionCommand> {

	private static final long serialVersionUID = 1L;
	
	
	private final String instrumentCode;
	private final String market;
	private final LocalDateTime dateTime;
	private final TransactionType transactionType;
	private final Money stockPrice;
	private final Long volume;

	
	public TransactionCommand(String instrumentCode, 
		String market,
		LocalDateTime dateTime,
		TransactionType transactionType,
		Money stockPrice,
		Long volume) {

		Assert.notNull(instrumentCode, "instrumentCode must not be null");
		Assert.notNull(market, "market must not be null");
		Assert.notNull(dateTime, "dateTime must not be null");
		Assert.notNull(transactionType, "transactionType must not be null");
		Assert.notNull(stockPrice, "stockPrice must not be null");
		Assert.notNull(volume, "volume must not be null");
		
		this.instrumentCode = instrumentCode;
		this.market = market;
		this.dateTime = dateTime;
		this.transactionType = transactionType;
		this.stockPrice = stockPrice;
		this.volume = volume;
	}
	

	public String getInstrumentCode() {

		return instrumentCode;
	}

	public String getMarket() {

		return market;
	}

	public LocalDateTime getDateTime() {

		return dateTime;
	}

	public TransactionType getTransactionType() {

		return transactionType;
	}

	public Money getStockPrice() {

		return stockPrice;
	}

	public Long getVolume() {

		return volume;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;

		if (!(o instanceof TransactionCommand)) {
			return false;
		}

		TransactionCommand that = (TransactionCommand) o;

		return new EqualsBuilder()
			.append(instrumentCode, that.getInstrumentCode())
			.append(market, that.getMarket())
			.append(dateTime, that.getDateTime())
			.append(transactionType, that.getTransactionType())
			.append(stockPrice, that.getStockPrice())
			.append(volume, that.getVolume())
			.isEquals();
	}

	@Override
	public int hashCode() {

		return Objects.hash(instrumentCode, market, dateTime, transactionType, stockPrice, volume);
	}

	@Override
	public int compareTo(TransactionCommand transactionCommand) {

		return dateTime.compareTo(transactionCommand.getDateTime());
	}
}

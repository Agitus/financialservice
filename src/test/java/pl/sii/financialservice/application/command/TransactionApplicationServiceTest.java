/*
 * Created on 06-04-2021 22:19 by Karol Kaczorowski
 */

package pl.sii.financialservice.application.command;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.openMocks;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import pl.sii.financialservice.common.Currency;
import pl.sii.financialservice.common.Money;
import pl.sii.financialservice.common.TransactionType;
import pl.sii.financialservice.domain.MarketInstrument;
import pl.sii.financialservice.domain.MarketInstrumentRepository;

@RunWith(JUnitParamsRunner.class)

public class TransactionApplicationServiceTest {

	private static final String MARKET = "GPW";
	private static final String INSTRUMENT_CODE = "KGHM";
	
	@Mock private MarketInstrumentRepository marketInstrumentRepository;
	private TransactionApplicationService transactionApplicationService;
	

	@Before
	public void setUp() {

		openMocks(this);

		transactionApplicationService = new TransactionApplicationService(marketInstrumentRepository);
	}
	
	@Test
	@Parameters(method = "processTransactionsParameters")
	public void shouldProcessTransactions(List<TransactionCommand> transactionList, Money profit) {
	
		// given
		MarketInstrument marketInstrument = new MarketInstrument(MARKET, INSTRUMENT_CODE);
		
		given(marketInstrumentRepository.findInstrument(anyString(), anyString()))
			.willReturn(Optional.of(marketInstrument));
	
		// when
		transactionApplicationService.cleanAndProcessTransactions(transactionList);
	
		// then
		assertEquals(profit, marketInstrument.getTotalYearlyProfitMap().get(2021));
	
	}

	@Test
	@Parameters(method = "correctSingleStockCostParameters")
	public void shouldCorrectSingleStockCost(Money cost, Long volume, Money singleStockCost) {

		// given

		// when
		Money result = transactionApplicationService.singleStockCost(cost, volume);

		// then
		assertEquals(singleStockCost, result);
	}

	@Test
	@Parameters(method = "correctTransactionTotalCostParameters")
	public void shouldCorrectTransactionTotalCost(Money cost, Money transactionTotalCost) {

		// given

		// when
		Money result = transactionApplicationService.transactionTotalCost(cost);

		// then
		assertEquals(transactionTotalCost, result);
	}

	@Test
	@Parameters(method = "correctTransactionExtraCostParameters")
	public void shouldCorrectTransactionExtraCost(Money cost, Money transactionExtraCost) {

		// given

		// when
		Money result = transactionApplicationService.transactionExtraCost(cost);

		// then
		assertEquals(transactionExtraCost, result);
	}


	private Object[] processTransactionsParameters() {

		return $(
			$(List.of(), null),
			$(
				List.of(
					createTransaction("2021-02-02", TransactionType.K, 100L, createPln(100L))
				), 
				null),
			$(
				List.of(
					createTransaction("2021-02-05", TransactionType.S, 100L, createPln(100L)),
					createTransaction("2020-02-02", TransactionType.K, 100L, createPln(100L))
				),
				createPln(-78L)
			),
			$(
				List.of(
					createTransaction("2021-02-05", TransactionType.K, 100L, createPln(10L)),
					createTransaction("2021-02-07", TransactionType.S, 150L, createPln(6L)),
					createTransaction("2021-02-02", TransactionType.K, 100L, createPln(1L))
				),
				createPln(291.54)
			),
			$(
				List.of(
					createTransaction("2021-02-05", TransactionType.K, 100L, createPln(10L)),
					createTransaction("2021-02-07", TransactionType.S, 50L, createPln(6L)),
					createTransaction("2021-02-08", TransactionType.S, 100L, createPln(2L)),
					createTransaction("2021-02-02", TransactionType.K, 100L, createPln(1L))
				),
				createPln(-110.95)
			),
			$(
				List.of(
					createTransaction("2021-02-05", TransactionType.K, 100L, createPln(0.02)),
					createTransaction("2021-02-07", TransactionType.S, 50L, createPln(0.01)),
					createTransaction("2021-02-08", TransactionType.S, 100L, createPln(0.05)),
					createTransaction("2021-02-02", TransactionType.K, 100L, createPln(0.01))
				),
				createPln(-7L)
			)
		);
	}

	private Object[] correctSingleStockCostParameters() {

		return $(
			$(createPln(1L), 1L, createPln(4L)),
			$(createPln(10L), 10L, createPln(1.3)),
			$(createPln(10000L), 100L, createPln(100.39)),
			$(createPln(10000000000L), 100000L, createPln(100390L)),
			$(createPln(1L), 100000000L, createPln(0.00000004))
		);
	}

	private Object[] correctTransactionTotalCostParameters() {

		return $(
			$(createPln(1L), createPln(4L)),
			$(createPln(4889616803L), createPln(4908686308.53)),
			$(createPln(0.00000001), createPln(3.00000001))
		);
	}

	private Object[] correctTransactionExtraCostParameters() {

		return $(
			$(createPln(1L), createPln(3L)),
			$(createPln(4889616803L), createPln(19069505.5317)),
			$(createPln(0.00000001), createPln(3L))
		);
	}
	
	private TransactionCommand createTransaction(String date, TransactionType transactionType, Long volume, Money stockPrice) {

		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime dateTime = LocalDate.parse(date, dateTimeFormatter).atStartOfDay();
		
		return new TransactionCommand(INSTRUMENT_CODE, MARKET, dateTime, transactionType, stockPrice, volume);
	}

	private Money createPln(Long value) {

		return Money.of(BigDecimal.valueOf(value), Currency.PLN);
	}

	private Money createPln(Double value) {

		return Money.of(BigDecimal.valueOf(value), Currency.PLN);
	}
}
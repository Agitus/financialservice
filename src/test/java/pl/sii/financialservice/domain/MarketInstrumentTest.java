/*
 * Created on 06-04-2021 19:42 by Karol Kaczorowski
 */

package pl.sii.financialservice.domain;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import pl.sii.financialservice.common.Currency;
import pl.sii.financialservice.common.Money;

@RunWith(JUnitParamsRunner.class)
public class MarketInstrumentTest {
	
	
	@Test
	public void shouldCorrectVolumeBoughtStocks() {
	
		// given
		MarketInstrument marketInstrument = new MarketInstrument(RandomStringUtils.random(5), RandomStringUtils.random(5));
		long volume1 = RandomUtils.nextLong(1, 1000000);
		long volume2 = RandomUtils.nextLong(1, 1000000);
		Money price1 = createPln(RandomUtils.nextDouble(0.000001f, 1000));
		Money price2 = createPln(RandomUtils.nextDouble(0.000001f, 1000));
		
		// when
		marketInstrument.buyStocks(volume1, price1, LocalDateTime.now());
		marketInstrument.buyStocks(volume2, price2, LocalDateTime.now());
	
		// then
		assertEquals(2, marketInstrument.getOwnedStocks().size());
		assertEquals(volume1+volume2, (long) marketInstrument.getVolume());
		assertEquals(volume1, (long) marketInstrument.getOwnedStocks().get(0).getVolume());
		assertEquals(volume2, (long) marketInstrument.getOwnedStocks().get(1).getVolume());
	}
	
	@Test
	@Parameters(method = "correctNetProfitParameters")
	public void shouldCalculateCorrectNetProfit(List<Pair<Long, Money>> buyTransactions, List<Pair<Long, Money>> sellTransactions, Money profit) {
	
		// given
		MarketInstrument marketInstrument = new MarketInstrument(RandomStringUtils.random(5), RandomStringUtils.random(5));
		
		// when
		buyTransactions.forEach( transaction -> 
			marketInstrument.buyStocks(transaction.getLeft(), transaction.getRight(), LocalDateTime.now())
		);
		sellTransactions.forEach( transaction ->
			marketInstrument.sellStocks(transaction.getLeft(), transaction.getRight(), LocalDateTime.now().getYear())
		);

		Money result = marketInstrument.getTotalYearlyProfitMap().get(LocalDateTime.now().getYear());

		// then
		assertEquals(profit, result);
	
	}

	private Object[] correctNetProfitParameters() {

		return $(
			$(List.of(), List.of(), null),
			$(List.of(Pair.of(100L, createPln(100L))), List.of(), null),
			$(
				List.of(Pair.of(100L, createPln(100L))),
				List.of(Pair.of(100L, createPln(10000L))),
				createPln(0L)
			),
			$(
				List.of(Pair.of(100L, createPln(1L)), Pair.of(100L, createPln(10L))),
				List.of(Pair.of(150L, createPln(900L))),
				createPln(300L)
			),
			$(
				List.of(Pair.of(100L, createPln(1L)), Pair.of(100L, createPln(10L))),
				List.of(Pair.of(50L, createPln(300L)), Pair.of(100L, createPln(200L))),
				createPln(-100L)
			),
			$(
				List.of(Pair.of(100L, createPln(0.01)), Pair.of(100L, createPln(0.02))),
				List.of(Pair.of(50L, createPln(0.5)), Pair.of(100L, createPln(5L))),
				createPln(3.5)
			)
		);
	}

	private Money createPln(Long value) {

		return Money.of(BigDecimal.valueOf(value), Currency.PLN);
	}

	private Money createPln(Double value) {

		return Money.of(BigDecimal.valueOf(value), Currency.PLN);
	}
}